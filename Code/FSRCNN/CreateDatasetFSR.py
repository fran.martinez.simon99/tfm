#Librerias
import cv2
import os

#Directorio con las imagenes sobre las que crearemos el dataset
OriginalImages="../zelda"
#Carpeta donde guardaremos las imagenes de entrada
InputImages="D:/aaaTFM/dataset/fsrcnn/images"
#Carpeta donde guardaremos las imagenes de salida
OutputImages="D:/aaaTFM/dataset/fsrcnn/output"

#Valor que representa la escala que usaremos para calcular el tamaño de las nuevas imagenes
Scale=2.0
#Tamaño de las imagenes de entrada
InputSize=21
#Tamaño de las imagenes de salida
OutputSize=42
#Valor de desplazamiento sobre las imagenes
Stride=14

#Para que las imagenes representen lo mismo de forma mas precisa, debemos calcular un padding
padding=int((OutputSize - InputSize) / 2.0)
#Variable auxiliar para definir el nombre de las variables
currentImage=0
#Iteramos sobre los elementos que encontremos en el directorio de las imagenes originales
# loop through the files in the directory
for file in os.listdir(OriginalImages):
	# Obtenemos la imagen de alta definicion
	OriginalImage = cv2.imread(OriginalImages + '/' + file)
	# Obtenemos el tamaño de la imagen de alta resolucion
	OriginalImageHeight, OriginalImageWidth, _ = OriginalImage.shape

	# Creamos la imagen de baja resolucion. Para esto, reducimos la imagen y, luego, la devolvemos a su tamaño original
	LowResolutionImage = cv2.resize(OriginalImage, (int(OriginalImageWidth / Scale), int(OriginalImageHeight / Scale)), interpolation=cv2.INTER_CUBIC)
	LowResolutionImage = cv2.resize(LowResolutionImage, (OriginalImageWidth, OriginalImageHeight), interpolation=cv2.INTER_CUBIC)

	# Iteramos sobre la imagen actual, para poder obtener subimagenes de ella
	for i in range(0, OriginalImageHeight - InputSize + 1, Stride):
		for j in range(0, OriginalImageWidth - InputSize + 1, Stride):
			# Sobre la imagen de baja resolucion, extraemos una porcion correspondiente a InputSizexInputSize
			InputImage = LowResolutionImage[i:i + InputSize, j:j + InputSize]
			# Sobre la imagen de alta resolucion, extraemos una porcion correspondiente a OutputSizexOutputSize. Ademas, usamos el padding para asegurarnos de que esten centradas correctamente
			OutputImage = LowResolutionImage[i + padding:i + padding + OutputSize, j + padding:j + padding + OutputSize]
			OutputImage = OriginalImage[int(i * Scale)+padding:int(i * Scale) + OutputSize + padding, int(j * Scale)+padding:int(j * Scale) + OutputSize + padding]
			# Guardamos la subimagen de entrada actual
			cv2.imwrite(InputImages + "/" + str(currentImage) + ".png", InputImage)
			# Guardamos la subimagen de salida actual
			cv2.imwrite(OutputImages + "/" + str(currentImage) + ".png", OutputImage)

			# Actualizamos la variable auxiliar
			currentImage += 1
