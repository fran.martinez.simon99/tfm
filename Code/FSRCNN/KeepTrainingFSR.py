#Librerias
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use("Agg")
import HDF5
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.models import load_model

#Ruta donde se encuentra el archivo con las imagenes de entrada
InputImages="D:/aaaTFM/dataset/fsrcnn/inputs.hdf5"
#Ruta donde se encuentra el archivo con las imagenes de salida
OutputImages="D:/aaaTFM/dataset/fsrcnn/outputs.hdf5"
#Tamaño de lotes para el entrenamiento
Batch=128
#NUmero de epocas para el entrenamiento
Epochs=10
#Verbosidad que queremos que haya durante el entrenamiento
Verbosity=1
#Ruta donde almacenarmeos el modelo
ModelRead="D:/aaaTFM/dataset/fsrcnn/model.model"
#Ruta donde guardamremos el nuevo modelo que obtengamos
ModelSave="D:/aaaTFM/dataset/fsrcnn/model2.model"
#Ruta donde almacenaremos el grafico con los resultados
Plot="D:/aaaTFM/dataset/fsrcnn/plot.png"
#Ruta donde almacenaremos la mejor epoca obtenida
BestEpoch="D:/aaaTFM/dataset/fsrcnn/bestEpoch.h5"
#Variable que controla si quieremos que los datos se introduzcan de forma aleatoria
DoShuffle=True

#Cargamos los datos de entrenamiento
inputs = HDF5.PrepareHDF5(InputImages, Batch)
outputs = HDF5.PrepareHDF5(OutputImages, Batch)

#Cargamos el modelo ya entrenado
model = load_model(ModelRead)
#Definimos un punto de guardado para almacenar siempre los datos de las mejor epoca conseguida
SaveBestEpochCallback = ModelCheckpoint(filepath=BestEpoch,monitor='loss',mode='min',save_best_only=True)
#Entrenamos el modelo
H = model.fit(HDF5.yieldData(inputs.generator(), outputs.generator()), steps_per_epoch=inputs.numImages // Batch, epochs=Epochs, verbose=Verbosity, shuffle=DoShuffle, callbacks=[SaveBestEpochCallback])

#Una vez acabado el entrenamiento, guardamos el modelo
model.save(ModelSave)

#Creamos una grafica con los resultados obtenidos, mostrando la perdida frente a las epocas
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, Epochs), H.history["loss"],label="loss")
plt.title("Loss over training")
plt.xlabel("Epoch")
plt.ylabel("Loss")
plt.legend()
plt.savefig(Plot)