#Librerias
from tensorflow.keras.models import load_model
import numpy as np
import cv2
import time

#Ruta desde la que leemos el modelo ya entrenado
ModelRead="D:/aaaTFM/dataset/fsrcnn/model5.model"
#Valor que representa la escala que usaremos para calcular el tamaño de las nuevas imagenes
Scale=3.0
#Tamaño de las imagenes de entrada
InputSize=21
#Tamaño de las imagenes de salida
OutputSize=63
#Ruta donde se encuentra la imagen sobre la que vamos a aplicar el modelo
InputImage="D:/aaaTFM/ImagesTime/Ghost.jpg"

#Cargamos el modelo
model = load_model(ModelRead)
#Para que las imagenes representen lo mismo de forma mas precisa, debemos calcular un padding
padding=int((OutputSize - InputSize) / 2.0)

#Comenzamos a medir el tiempo
StartTime = time.time()

#Leemos la imagen de prueba
image = cv2.imread(InputImage)
#Obtenemos el tamaño de la imagen de prueba
imageHeight, imageWidth, _ = image.shape
#Obtenemos el tamaño de la imagen reescalada
resultHeight=int(imageHeight*Scale)
resultWidth=int(imageWidth*Scale)
#Definimos la imagen de alta resolucion en base a las dimensiones recien calculadas
ResultImage = np.zeros([resultHeight, resultWidth,image.shape[2]])

#Recorremos la imagen bicubica
for i in range(0, resultHeight - (InputSize-1), InputSize):
	for j in range(0, resultWidth - (InputSize-1), InputSize):
		#Extraemos la subimagen de la iteracion actual
		SubImage = image[i:i + InputSize, j:j + InputSize].astype("float32")
		#Si hemos alcanzado un borde, pasamos a la siguiente iteracion
		if SubImage.shape[0]<InputSize or SubImage.shape[1]<InputSize: continue
		#Aplicamos el modelo sobre la subimagen
		SubImageResult = model.predict(np.expand_dims(SubImage, axis=0))
		#Aplicamos el resultado sobre la imagen final
		ResultImage[i + padding:i + padding + OutputSize,j + padding:j + padding + OutputSize] = SubImageResult

#Imprimimos el tiempo que ha tardado
print("Time: " + str(time.time() - StartTime))