#Librerias
from tensorflow.keras.optimizers import Adam
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use("Agg")
import HDF5
from model import model
from tensorflow.keras.callbacks import ModelCheckpoint

#Ruta donde se encuentra el archivo con las imagenes de entrada
InputImages="D:/aaaTFM/dataset/fsrcnn/inputs.hdf5"
#Ruta donde se encuentra el archivo con las imagenes de salida
OutputImages="D:/aaaTFM/dataset/fsrcnn/outputs.hdf5"
#Tamaño de lotes para el entrenamiento
Batch=128
#NUmero de epocas para el entrenamiento
Epochs=10
#Variable que indica el tamaño de las imagenes de entrada
InputSize=21
#Variable que indica el numero de canales de las imagenes
Channels=3
#Numero de filtros de la primera capa
N1=56
#Tamaño de filtros de la primera capa
F1=5
#Numero de filtros de la segunda capa
N2=12
#Tamaño de filtros de la segunda capa
F2=1
#Numero de filtros de la tercera capa
N3=12
#Tamaño de filtros de la tercera capa
F3=3
#Numero de filtros de la cuarta capa
N4=56
#Tamaño de filtros de la cuarta capa
F4=1
#Tamaño de filtros de la quinta capa
F5=9
#Numero de capas de mapeo
M=4
#Verbosidad que queremos que haya durante el entrenamiento
Verbosity=1
#Ruta donde almacenarmeos el modelo
Model="D:/aaaTFM/dataset/fsrcnn/model.model"
#Ruta donde almacenaremos el grafico con los resultados
Plot="D:/aaaTFM/dataset/fsrcnn/plot.png"
#Ruta donde almacenaremos la mejor epoca obtenida
BestEpoch="D:/aaaTFM/dataset/fsrcnn/bestEpoch.h5"
#Variable que controla si quieremos que los datos se introduzcan de forma aleatoria
DoShuffle=True

#Cargamos los datos de entrenamiento
inputs = HDF5.PrepareHDF5(InputImages, Batch)
outputs = HDF5.PrepareHDF5(OutputImages, Batch)

#Definimos el optimizador
opt = Adam(lr=0.001, decay=0.001 / Epochs)
#Cargamos el modelo con los parametros adecuados
model = model.build(width=InputSize, height=InputSize, depth=Channels, m=M,N1=N1, F1=F1, N2=F2, F2=F2,N3=N3, F3=F3, N4=F4, F4=F4, F5=F5)
#Definimos la funcion de perdida del modelo y le añadimos el optimizador anterior
model.compile(loss="mse", optimizer=opt,metrics="mse")
#Definimos un punto de guardado para almacenar siempre los datos de las mejor epoca conseguida
SaveBestEpochCallback = ModelCheckpoint(filepath=BestEpoch,monitor='loss',mode='min',save_best_only=True)
#Entrenamos el modelo
H = model.fit(HDF5.yieldData(inputs.generator(), outputs.generator()), steps_per_epoch=inputs.numImages // Batch, epochs=Epochs, verbose=Verbosity, shuffle=DoShuffle, callbacks=[SaveBestEpochCallback])

#Una vez acabado el entrenamiento, guardamos el modelo
model.save(Model)

#Creamos una grafica con los resultados obtenidos, mostrando la perdida frente a las epocas
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, Epochs), H.history["loss"],label="loss")
plt.title("Loss over training")
plt.xlabel("Epoch")
plt.ylabel("Loss")
plt.legend()
plt.savefig(Plot)