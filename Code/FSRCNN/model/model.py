#Librerias
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Conv2DTranspose
from tensorflow.keras import backend as K
from keras.layers.advanced_activations import PReLU

#Clase que define el modelo de nuestra red neuronal FSRCNN
class model:
	#Metodo para construir desde la clase de entrenamiento
	@staticmethod
	def build(width, height, depth, m, N1, F1, N2, F2, N3, F3, N4, F4, F5):
		#Priemro, debemos comprobar el formato de imagen de nuestro keras
		if K.image_data_format() == "channels_first":
			#Si los canales van primero, debemos ajustar la entrada de la red para estos valores
			InputFormat = (depth, height, width)
		else:
			#Si los canales no van primero, debemos ajustar la entrada de la red para estos valores
			InputFormat = (height, width, depth)

		#Inicializamos el modelo como secuencial
		model = Sequential()
		#Definimos la primera capa, la de extraccion
		model.add(Conv2D(N1, (F1, F1),padding='same', kernel_initializer="he_normal", input_shape=InputFormat))
		model.add(PReLU())
		#Definimos la segunda capa, la de reducción
		model.add(Conv2D(N2, (F2, F2), padding='same', kernel_initializer="he_normal"))
		model.add(PReLU())
		#Definimos las capas de mapeo
		for i in range(0,m):
			model.add(Conv2D(N3, (F3, F3), padding='same', kernel_initializer="he_normal"))
			model.add(PReLU())
		#Definimos la capa de expnasion
		model.add(Conv2D(N4, (F4, F4), padding='same', kernel_initializer="he_normal"))
		model.add(PReLU())
		#Definimos la capa final, la de deconvolucion
		model.add(Conv2DTranspose(depth, (F5, F5), strides=(2, 2), padding='same'))

		#Devolvemos el modelo
		return model