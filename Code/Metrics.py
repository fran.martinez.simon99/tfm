#Librerias
from math import log10, sqrt
import cv2
import numpy as np
from tensorflow.keras.models import load_model
import os

#Funcion para medir el PSNR
def PSNR(img1, img2):
	#Calculamos el error cuadratico medio
	mse = np.mean((img1 - img2) ** 2)
	#Comprobamos que las imagenes no sean exactamente iguales
	if (mse == 0):
		return 100
	#Calculamos el psnr
	maxPixel = 255.0
	psnr = 20 * log10(maxPixel / sqrt(mse))
	return psnr

#Ruta donde se encuentra el archivo con las imagenes de entrada
InputImages="D:/aaaTFM/ImagesPruebas"
#Ruta donde se encuentra el modelo
ModelPath = "D:/aaaTFM/dataset/Final/Train-2/model3.model"
#Valor que representa la escala que usaremos para calcular el tamaño de las nuevas imagenes
Scale = 2
#Tamaño de las imagenes de entrada
InputSize=33
#Tamaño de las imagenes de salida
OutputSize=21

#Cargamos el modelo
model = load_model(ModelPath)
#Para que las imagenes representen lo mismo de forma mas precisa, debemos calcular un padding
padding = int((InputSize - OutputSize) / 2.0)

#Variable para controlar que imagen es la actual
current=0
#Iteramos sobre las imagenes de prueba
for file in os.listdir(InputImages):
	#Leemos la imagen actual
	Original = cv2.imread(InputImages + '/' + file)
	#Obtenemos el tamaño de la imagen de prueba
	OriginalImageHeight, OriginalImageWidth, _ = Original.shape
	#Reducimos su tamaño y luego lo devolvemos al original, para poder calculcar el PSNR de interpolacion bicubica
	BicubicImage = cv2.resize(Original, (int(OriginalImageWidth / Scale), int(OriginalImageHeight / Scale)), interpolation=cv2.INTER_CUBIC)
	BicubicImage = cv2.resize(BicubicImage, (OriginalImageWidth, OriginalImageHeight), interpolation=cv2.INTER_CUBIC)
	#Calculamos el PSNR
	ValueBicubic = PSNR(Original, BicubicImage)

	#Definimos la imagen de alta resolucion en base a las dimensiones de la imagen bicubica
	ResultImage = np.zeros(BicubicImage.shape)
	#Obtenemos el tamaño de la imagen reescalada
	resultHeight, resultWidth, _ = ResultImage.shape

	#Recorremos la imagen bicubica
	for i in range(0, resultHeight - (InputSize - 1), OutputSize):
		for j in range(0, resultWidth - (InputSize - 1), OutputSize):
			#Extraemos la subimagen de la iteracion actual
			SubImage = BicubicImage[i:i + InputSize, j:j + InputSize].astype("float32")
			#Aplicamos el modelo sobre la subimagen
			SubImageResult = model.predict(np.expand_dims(SubImage, axis=0))
			#Aplicamos el resultado sobre la imagen final
			ResultImage[i + padding:i + padding + OutputSize, j + padding:j + padding + OutputSize] = SubImageResult

	#Extraemos los bordes de informacion perrida de la imagen resultado
	CroppedResult = ResultImage[padding:resultHeight - ((resultHeight % InputSize) + padding),padding:resultWidth - ((resultWidth % InputSize) + padding)]
	CroppedResult = np.clip(CroppedResult, 0, 255).astype("uint8")

	#Extraemos los bordes de informacion perrida de la imagen original
	CroppedOriginal = Original[padding:resultHeight - ((resultHeight % InputSize) + padding), padding:resultWidth - ((resultWidth % InputSize) + padding)]
	CroppedOriginal = np.clip(CroppedOriginal, 0, 255).astype("uint8")

	#Calculamos el PSNR
	ValueResult = PSNR(CroppedOriginal, CroppedResult)

	print("Bicubic PSNR- "+str(current)+": "+str(ValueBicubic))
	print("Result PSNR- "+str(current)+": "+str(ValueResult))

	#Actualizamos nuestra variable auxiliar
	current+=1
