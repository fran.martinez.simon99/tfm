#Librerias
from tensorflow.keras.models import load_model
import numpy as np
import cv2

#Ruta desde la que leemos el modelo ya entrenado
ModelRead="D:/aaaTFM/dataset/Final/Train-1/model.model"
#Valor que representa la escala que usaremos para calcular el tamaño de las nuevas imagenes
Scale=2.0
#Tamaño de las imagenes de entrada
InputSize=33
#Tamaño de las imagenes de salida
OutputSize=21
#Ruta donde se encuentra la imagen sobre la que vamos a aplicar el modelo
InputImage="../prueba.jpeg"
#Ruta donde almacenaremos la imagen bicubica
BicubicImageSave="D:/aaaTFM/dataset/Final/Train-1/bicubicImage.png"
#Ruta donde almacenaremos la imagen del modelo
ResultImageSave="D:/aaaTFM/dataset/Final/Train-1/result.png"

#Cargamos el modelo
model = load_model(ModelRead)
#Para que las imagenes representen lo mismo de forma mas precisa, debemos calcular un padding
padding=int((InputSize - OutputSize) / 2.0)

#Leemos la imagen de prueba
image = cv2.imread(InputImage)
#Obtenemos el tamaño de la imagen de prueba
imageHeight, imageWidth, _ = image.shape

#Aumentamos, mediante interpolacion bicubica, el tamaño de la imagen de prueba para que se ajuste a la escala
BicubicImage = cv2.resize(image, (int(imageWidth * Scale), int(imageHeight * Scale)),interpolation=cv2.INTER_CUBIC)
#Guardamos la imagen bicubica para comparar resultados
cv2.imwrite(BicubicImageSave, BicubicImage)

#Definimos la imagen de alta resolucion en base a las dimensiones de la imagen bicubica
ResultImage = np.zeros(BicubicImage.shape)
#Obtenemos el tamaño de la imagen reescalada
resultHeight, resultWidth, _ = ResultImage.shape

#Recorremos la imagen bicubica
for i in range(0, resultHeight - (InputSize-1), OutputSize):
	for j in range(0, resultWidth - (InputSize-1), OutputSize):
		#Extraemos la subimagen de la iteracion actual
		SubImage = BicubicImage[i:i + InputSize, j:j + InputSize].astype("float32")
		#Aplicamos el modelo sobre la subimagen
		SubImageResult = model.predict(np.expand_dims(SubImage, axis=0))
		#Aplicamos el resultado sobre la imagen final
		ResultImage[i + padding:i + padding + OutputSize,j + padding:j + padding + OutputSize] = SubImageResult

#Almacenamos la imagen final
cv2.imwrite(ResultImageSave, ResultImage)

