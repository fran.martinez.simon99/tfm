#Librerias
import cv2
import HDF5
import os

#Ruta donde se encuentra el archivo con las imagenes de entrada
InputImagesHDF5="D:/aaaTFM/dataset/scaleVer4/inputs.hdf5"
#Ruta donde se encuentra el archivo con las imagenes de salida
OutputImagesHDF5="D:/aaaTFM/dataset/scaleVer4/outputs.hdf5"
#Ruta donde se encuentra el archivo con las imagenes de entrada
InputImages="D:/aaaTFM/dataset/scaleVer4/images"
#Ruta donde se encuentra el archivo con las imagenes de salida
OutputImages="D:/aaaTFM/dataset/scaleVer4/output"
#Tamaño de las imagenes de entrada
InputSize=33
#Tamaño de las imagenes de salida
OutputSize=21
#Numero de canales de las imagenes
Channels=3

#Caclulamos cuantas imagenes hay en el dataset
total=0
for (ImageInput,ImageOutput) in zip(os.listdir(InputImages),os.listdir(OutputImages)):
	total+=1

#Nos preparamos para almacenar en HDF5 las imagenes
inputHDF5 = HDF5.CreateHDF5((total, InputSize,InputSize, Channels), InputImagesHDF5)
outputHDF5 = HDF5.CreateHDF5((total,OutputSize, OutputSize, Channels), OutputImagesHDF5)

#Iteramos sobre las imagenes del dataset
for (inputPath, outputPath) in zip(os.listdir(InputImages),os.listdir(OutputImages)):
	#Leemos el par de imagenes actual
	inputImage = cv2.imread(inputPath)
	outputImage = cv2.imread(outputPath)
	#Lo convertimos a hdf5
	inputHDF5.add([inputImage], [-1])
	outputHDF5.add([outputImage], [-1])

