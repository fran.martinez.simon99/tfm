#Librerias
from tensorflow.keras import backend as K
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Activation

#Clase que define el modelo de nuestra red neuronal SRCNN
class model:
	#Metodo para construir desde la clase de entrenamiento
	@staticmethod
	def build(width, height, depth,N1,F1,N2,F2,F3):
		#Priemro, debemos comprobar el formato de imagen de nuestro keras
		if K.image_data_format() == "channels_first":
			#Si los canales van primero, debemos ajustar la entrada de la red para estos valores
			InputFormat = (depth, height, width)
		else:
			# Si los canales no van primero, debemos ajustar la entrada de la red para estos valores
			InputFormat = (height, width, depth)

		#Inicializamos el modelo como secuencial
		model = Sequential()
		#Definimos la primera capa, la de extraccion,
		model.add(Conv2D(N1, (F1, F1), kernel_initializer="he_normal", input_shape=InputFormat))
		model.add(Activation("relu"))
		model.add(Conv2D(N2, (F2, F2), kernel_initializer="he_normal"))
		model.add(Activation("relu"))
		model.add(Conv2D(depth, (F3, F3), kernel_initializer="he_normal"))
		model.add(Activation("relu"))

		#Devolvemos el modelo
		return model