Estructura del proyecto:

* La carpeta "Code" contiene el código (en Python). Dentro de esta carpeta, el código relativo a FSRCNN puede encontrarse en una carpeta homónima.
* La carpeta "Dataset" contiene las imágenes originales sobre las que se ha construido el Dataset. Sin embargo, debido a la gran cantidad de memoria que ocupan, no hemos subidos los Datasets generados a partir de este Dataset original.
* La carpeta "ModeloFinal" contiene el modelo resultante del último entrenamiento.
* La carpeta "Models" contiene los resultados de varios entrenamientos. Además, la prueba de entrenamiento realizada con FSRCNN se encuentra en esta carpeta.
